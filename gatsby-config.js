const path = require('path')
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

module.exports = ({ 
        optionsPath = `${__dirname}/options/`, 
        staticPath = path.join(__dirname, `src`, `static`),
        themePath = `${__dirname}/src/theme`,
        stylePath = `/style`
    }) => ({
    siteMetadata: {
        siteTitle: `Gatsby Theme Drupal Minimal`,
        siteTitleAlt: `Minimal Gatsby-Drupal Theme`,
        siteHeadline: `Drupal - Gatsby`,
        siteDescription: `Minimal theme for Drupal to Gatsby communication`,
        siteImage: `/hero.jpeg`,
        author: `Ryan Stivers`,
      },
      trailingSlash: `never`,
    plugins: [
        {
            resolve: `gatsby-source-drupal`,
            options: {
                baseUrl: process.env.DRUPAL_URL,
                apiBase: 'jsonapi', // endpoint of Drupal server,
                fastBuilds: true,
                headers: {
                    'api-key': process.env.JSON_API_KEY,
                },
            },
        },
        {
            resolve: `gatsby-plugin-page-creator`,
            options: {
                path: optionsPath,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
              name: `options`,
              path: optionsPath,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
              name: `static`,
              path: staticPath,
            },
        },
        {
            resolve: `gatsby-plugin-theme-ui`,
            options: {
                preset: themePath,
            },
        },
        {
            resolve: `gatsby-theme-style-guide`,
            options: {
                basePath: stylePath,
            },
        },
        {
            resolve: `gatsby-transformer-yaml`,
            options: {
                typeName: "Option",
            },
        },
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sharp`, 
        `gatsby-plugin-image`,
    ],
})
const fs = require("fs")
const path = require(`path`)

// Make sure the static directory exists
exports.onPreBootstrap = ({ reporter }, options) => {
  const optionsPath = options.optionsPath || `${__dirname}/options/`

  if (!fs.existsSync(optionsPath)) {
    reporter.info(`creating the ${optionsPath} directory`)
    fs.mkdirSync(optionsPath)
  }
}

// Defining the Option type
exports.createSchemaCustomization = ({ actions }) => {
    actions.createTypes(`
        type linkOption implements Node @dontInfer {
            id: ID!
            name: String!
            url: String!
            type: String!
            display: Boolean!
        }
    `)
}

exports.createPages = async ({ graphql, actions, reporter }, options) => {
    const { createPage } = actions  
    const articlePath = require.resolve('./src/templates/article')
    const articlesPath = require.resolve('./src/templates/articles')
    const recipePath = require.resolve('./src/templates/recipe')
    const recipesPath = require.resolve('./src/templates/recipes')
    const homepagePath = require.resolve('./src/templates/homepage')
    const basePath = options.basePath || '/'
    
    const articlesQuery = await graphql(`
      query {
        allNodeArticle {
          edges {
            node {
              id
              title
              path {
                alias
              }
            }
          }
        }
      }
    `)

    const recipesQuery = await graphql(`
      query {
        allNodeRecipe {
          edges {
            node {
              id
              title
              path {
                alias
              }
            }
          }
        }
      }
    `)

    if (articlesQuery.errors) {
        reporter.panic("error loading articles", articlesQuery.errors)
        return
    }

    if (recipesQuery.errors) {
      reporter.panic("error loading recipes", recipesQuery.errors)
      return
    }
    
    articlesQuery.data.allNodeArticle.edges.forEach((articleData) => {
      createPage({
        path: articleData.node.path.alias,
        component: articlePath,
        context: {
          ArticleId: articleData.node.id
        },
      })
    });
    
    createPage({
      path: '/articles',
      component: articlesPath,
    })

    recipesQuery.data.allNodeRecipe.edges.forEach((recipeData) => {
      createPage({
        path: recipeData.node.path.alias,
        component: recipePath,
        context: {
          RecipeId: recipeData.node.id
        },
      })
    });

    createPage({
      path: '/recipes',
      component: recipesPath,
    })

    createPage({
      path: basePath,
      component: homepagePath,
    })
  }
  
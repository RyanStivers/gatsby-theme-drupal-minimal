import * as React from "react"
import { useColorMode, IconButton } from 'theme-ui'

const ColorModeButton = ({ props }) => {
    const [mode, setMode] = useColorMode()
  
    return (
      <IconButton
        {...props}
        onClick={(e) => {
          const next = mode === 'dark' ? 'light' : 'dark'
          setMode(next)
        }}
        aria-label="Toggle dark mode"
       >
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            width="24"
            height="24"
            fill="currentcolor">
            <circle
                r={11}
                cx={12}
                cy={12}
                fill="none"
                stroke="currentcolor"
                strokeWidth={2}
            />
        </svg>
       </IconButton>
    )
  }

export default ColorModeButton
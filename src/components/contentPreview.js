import React from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { GatsbyImage } from "gatsby-plugin-image"
import { Card, Container, Box, Heading, Flex, Badge, Text } from "theme-ui"

const ContentPreview = ({ title, path, image, alt, summary }) => (
    <Container>
        <Card sx={{ background: 'gray.0', borderRadius: '8px', margin: '40px', boxShadow: 'rgb(0 0 0 / 10%) 0px 8px 16px -4px, rgb(0 0 0 / 10%) 0px 0px 8px -3px', }}>
            <Link to={path} activeClassName="active" sx={{ color: '#fff', 'text-decoration': 'none' }}>
                <GatsbyImage 
                    image={image}
                    alt={alt} 
                    style={{ borderTopLeftRadius: '8px', borderTopRightRadius: '8px' }}
                />
            </Link>
            <Box sx={{ p: '3' }}>
                <Heading variant='cardHeading' as='h2' mb={2}> {title} </Heading>
                <Text mb={3} dangerouslySetInnerHTML={{ __html: summary }} />
                <Flex>
                    <Badge mr={1}>Food</Badge><Badge mr={1}>Recipe</Badge><Badge mr={1}>Article</Badge>
                </Flex>
            </Box>
        </Card>
    </Container>
)

ContentPreview.propTypes = {
    title: PropTypes.string.isRequired,
    path: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
};

export default ContentPreview;
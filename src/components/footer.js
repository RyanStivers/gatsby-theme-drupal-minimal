import * as React from "react"
import { useStaticQuery, graphql } from "gatsby"
import { Box, NavLink, Divider } from "theme-ui"


const Footer = ({ author }) => {

const data = useStaticQuery(graphql`
    query {
        allOption(filter: {type: {eq: "footerLink"}, display: {eq: true}}) {
            edges {
                node {
                    name
                    url
                }
            }
        }
    }
`);

const links = data.allOption.edges;

  return (
    <Box
        sx={{
            fontSize: 1,
            width: '100%',
            variant: 'layout.footer',
        }}>
        <Box
            sx={{
                display: 'grid',
                'grid-template-rows': 'repeat(4, 32px)',
                'grid-template-columns': ['repeat(2, 1fr)', 'repeat(4, 1fr)'],
                'grid-auto-flow': 'column',
                px: 2,
                py: 4,
            }}>
            {links.map(link => ( 
                <NavLink href={link.node.url} sx={{ variant: 'styles.navlink', p: 2 }}>
                    {link.node.name}
                </NavLink>
            ))}
        </Box>
        <Divider sx={{ color: 'gray.4' }}/>
        <Box
            sx={{
            display: 'flex',
            'justify-content': 'flex-end',
            p: 3,
            }}>
            <div sx={{ mx: 1 }} />
            © {new Date().getFullYear()} &middot; { author }
        </Box>
    </Box>
  )
}

export default Footer
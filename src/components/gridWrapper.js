import * as React from "react"
import { Grid, Box } from "theme-ui"

const GridWrapper = ({ children }) => {
  return (
    <Grid gap={2} columns={[3, '1fr 3fr 1fr']}>
        <Box />
        <Box>{children}</Box>
        <Box />
    </Grid>
  )
}

export default GridWrapper
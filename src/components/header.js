import * as React from "react"
import { useStaticQuery, graphql } from "gatsby"
import ColorModeButton from "./colorModeButton"
import { Flex, NavLink, Box } from "theme-ui"

const Header = ({ siteTitle }) => {

  const data = useStaticQuery(graphql`
    query {
        allOption(filter: {type: {eq: "headerLink"}, display: {eq: true}}) {
            edges {
                node {
                    name
                    url
                }
            }
        }
    }
  `);

  const links = data.allOption.edges;

  return (
    <Box sx={{ width: '100%', variant: 'layout.header' }}>
      <Flex as="nav" sx={{justifyContent: 'space-between'}}>
        <Flex sx={{ alignItems: 'center' }}>
          <NavLink href="/" sx={{ p: '3', display: 'flex', flexDirection: 'row', alignItems: 'center', fontSize: '4', color: 'primary'}}>
            &#8477;
          </NavLink>
          {links.map(link => ( 
              <NavLink href={link.node.url} sx={{ variant: 'styles.navlink', p: 2 }}>
                  {link.node.name}
              </NavLink>
          ))}
        </Flex>
        <Box sx={{ display: 'flex', placeItems: 'center' }}>
          <ColorModeButton />
        </Box>
      </Flex>
    </Box>
  )
}

export default Header
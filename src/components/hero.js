import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import { GatsbyImage } from "gatsby-plugin-image"

const Hero = () => {

    const data = useStaticQuery(graphql`
    {
        file(relativePath: {eq: "hero.jpeg"}) {
            childImageSharp {
                gatsbyImageData(
                    placeholder: BLURRED
                    formats: [WEBP]
                    aspectRatio: 2.5
                    layout: FULL_WIDTH
                    transformOptions: {fit: COVER, cropFocus: CENTER}
                    webpOptions: {quality: 100}
                )
            }
        }
    }
  `)

    return (
        <section>
            <GatsbyImage
                image={data.file.childImageSharp.gatsbyImageData}
                alt="Mountainous Terrain"
            />
        </section>
    )
}

export default Hero
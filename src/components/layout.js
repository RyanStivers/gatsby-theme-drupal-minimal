/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/how-to/querying-data/use-static-query/
 */

import * as React from "react"
import { useStaticQuery, graphql } from "gatsby"
import { Container, Box } from "theme-ui"

import Header from "./header"
import Footer from "./footer"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          siteTitle
          author
        }
      }
    }
  `)
  
  return (
    <>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          minHeight: '100vh',
          variant: 'layout.root',
        }}>
        <Header siteTitle={data.site.siteMetadata?.siteTitle || `Title`}/>
        <Box
          sx={{
            width: '100%',
            flex: '1 1 auto',
            variant: 'layout.main',
          }}>
          {children}
        </Box>
        <Footer author={data.site.siteMetadata?.author || `Author`}/>
      </Box>
    </>
  )
}

export default Layout
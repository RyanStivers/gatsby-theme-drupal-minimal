import React from "react";
import PropTypes from 'prop-types';
import { graphql } from "gatsby";

import Layout from "../components/layout";
import { Themed } from "@theme-ui/mdx"
import { Box, Container, Image } from "theme-ui"

const Article = ({ data }) => {
    const article = data.nodeArticle;

    return (
        <Layout>
            <Container
                sx={{ background: 'gray.0', 'border-radius': '8px', 'margin': '20px auto' }}
            >
            <Themed.h1>{article.title}</Themed.h1>
            {/* use Gatsby image functionality here instead */}
            <Image 
                src = {article.relationships.field_media_image.relationships.field_media_image.localFile.publicURL}
                alt = {article.relationships.field_media_image.field_media_image.alt}
                variant = "article"
            />
            <Box dangerouslySetInnerHTML={{ __html: article.body.processed }}/>
            </Container>
        </Layout>
    );
};

Article.propTypes = {
    data: PropTypes.object.isRequired,
};

export const query = graphql`
    query($ArticleId : String!) {
        nodeArticle(id: { eq: $ArticleId }) {
            title
            id
            body {
                processed
            }
            relationships {
                field_media_image {
                    field_media_image {
                        alt
                    }
                    relationships {
                        field_media_image {
                            localFile {
                                publicURL
                            }
                        }
                    }
                }
            }
        }
    }
`;

export default Article;
import React from "react";
import { graphql, useStaticQuery } from "gatsby";

import Layout from "../components/layout";
import Seo from "../components/seo";
import ContentPreview from "../components/contentPreview";

import { Themed } from "@theme-ui/mdx"

const ArticlesTemplate = () => {

    const data = useStaticQuery(graphql`
    query {
        allNodeArticle(sort: {created: DESC}) {
          edges {
            node {
              id
              title
              body {
                summary
                processed
              }
              path {
                alias
              }
              relationships {
                field_media_image {
                  field_media_image {
                    alt
                  }
                  relationships {
                    field_media_image {
                      localFile {
                        childImageSharp {
                          gatsbyImageData(
                            placeholder: BLURRED
                            formats: WEBP
                            aspectRatio: 1.75
                            layout: FULL_WIDTH
                            transformOptions: {fit: COVER, cropFocus: CENTER}
                          )
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `);

    const articles = data.allNodeArticle.edges;

    return (
        <Layout>
            <Seo title="Articles" />
            <Themed.h1>Articles</Themed.h1>
            {articles.map(article => (
                <ContentPreview
                    key={article.node.id}
                    title={article.node.title}
                    path={article.node.path.alias}
                    image={article.node.relationships.field_media_image.relationships.field_media_image.localFile.childImageSharp.gatsbyImageData}
                    alt={article.node.relationships.field_media_image.field_media_image.alt}
                    summary={article.node.body.summary ? article.node.body.summary : article.node.body.processed.substring(0, 200)+"..."}
                />
            ))}
        </Layout>
    )
}

export default ArticlesTemplate;
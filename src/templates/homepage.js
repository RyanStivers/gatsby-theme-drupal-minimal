import React from "react"
import Layout from "../components/layout";
import Seo from "../components/seo";
import Hero from "../components/hero";

const HomeTemplate = () => {   
    return (
        <Layout>
            <Seo title="Drupal->Gatsby Minimal" description="Minimal theme to communicate with a drupal deployment running Umami demo"/>           
            <Hero />
        </Layout>
    )
}

export default HomeTemplate

import React from "react";
import PropTypes from 'prop-types';
import { graphql } from "gatsby";

import Layout from "../components/layout";
import { Themed } from "@theme-ui/mdx"
import { Box, Container, Image, Divider, Grid } from "theme-ui"

const Recipe = ({ data }) => {
    const recipe = data.nodeRecipe;

    return (
        <Layout>
            <Container
                sx={{ background: 'gray.0', 'border-radius': '8px', 'margin': '20px auto' }}
            >
                <Themed.h1>{recipe.title}</Themed.h1>
                <Image 
                    src = {recipe.relationships.field_media_image.relationships.field_media_image.localFile.publicURL}
                    alt = {recipe.relationships.field_media_image.field_media_image.alt}
                    variant = "article"
                />
                <Grid gap={2} columns={[2, null, 4]} sx={{ textAlign: 'center' }}>
                    <Box>
                        Difficulty: <text dangerouslySetInnerHTML={{ __html: recipe.field_difficulty }}/>
                    </Box>
                    <Box> 
                        Cooking Time: <text dangerouslySetInnerHTML={{ __html: recipe.field_cooking_time }}/>
                    </Box>
                    <Box>
                        Prep Time: <text dangerouslySetInnerHTML={{ __html: recipe.field_preparation_time }}/>
                    </Box>
                    <Box>
                        Servings: <text dangerouslySetInnerHTML={{ __html: recipe.field_number_of_servings }}/>
                    </Box>
                </Grid>
                <Divider sx={{ color: 'gray.4' }} />
                <Grid gap={50} columns={2}>
                    <Box dangerouslySetInnerHTML={{ __html: recipe.field_recipe_instruction.processed }}/>
                    <Box>
                        {recipe.field_ingredients.map(ingredient => ( 
                            <Container dangerouslySetInnerHTML={{ __html: ingredient}}/>
                        ))}
                    </Box>
                </Grid>
            </Container>
        </Layout>
    );
};

Recipe.propTypes = {
    data: PropTypes.object.isRequired,
};

export const query = graphql`
    query($RecipeId : String!) {
        nodeRecipe(id: { eq: $RecipeId }) {
            title
            id
            field_summary {
                processed
            }
            field_recipe_instruction {
                processed
            }
            field_ingredients
            field_difficulty
            field_cooking_time
            field_number_of_servings
            field_preparation_time
            relationships {
                field_media_image {
                    field_media_image {
                        alt
                    }
                    relationships {
                        field_media_image {
                            localFile {
                                publicURL
                            }
                        }
                    }
                }
            }
        }
    }
`;

export default Recipe;
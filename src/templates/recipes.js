import React from "react";
import { graphql, useStaticQuery } from "gatsby";

import Layout from "../components/layout";
import Seo from "../components/seo";
import ContentPreview from "../components/contentPreview";

import { Themed } from "@theme-ui/mdx"

const RecipesTemplate = () => {

    const data = useStaticQuery(graphql`
        query MyQuery {
            allNodeRecipe(sort: {created: DESC}) {
                edges {
                    node {
                        id
                        title
                        field_recipe_instruction {
                            processed
                        }
                        field_summary {
                            processed
                        }
                        field_ingredients
                        field_difficulty
                        field_cooking_time
                        field_number_of_servings
                        field_preparation_time
                        path {
                            alias
                        }
                        relationships {
                            field_media_image {
                                field_media_image {
                                    alt
                                }
                                relationships {
                                    field_media_image {
                                        localFile {
                                            childImageSharp {
                                              gatsbyImageData(
                                                placeholder: BLURRED
                                                formats: WEBP
                                                aspectRatio: 1.75
                                                layout: FULL_WIDTH
                                                transformOptions: {fit: COVER, cropFocus: CENTER}
                                              )
                                            }
                                        }
                                    }
                                }
                            }
                            field_recipe_category {
                                name
                            }
                            field_tags {
                                name
                            }
                        }
                    }
                }
            }
        }
    `);

    const recipes = data.allNodeRecipe.edges;

    return (
        <Layout>
            <Seo title="Recipes" />
            <Themed.h1>Recipes</Themed.h1>
            {recipes.map(recipe => (
                <ContentPreview
                    key={recipe.node.id}
                    title={recipe.node.title}
                    path={recipe.node.path.alias}
                    image={recipe.node.relationships.field_media_image.relationships.field_media_image.localFile.childImageSharp.gatsbyImageData}
                    alt={recipe.node.relationships.field_media_image.field_media_image.alt}
                    summary={recipe.node.field_summary.processed ? recipe.node.field_summary.processed : recipe.node.field_recipe_instruction.processed.substring(0, 200)+"..."}
                    instructions={recipe.node.field_recipe_instruction.processed}
                    ingredients={recipe.node.field_ingredients}
                    difficulty={recipe.node.field_difficulty}
                    servings={recipe.node.field_number_of_servings}
                    cookingTime={recipe.node.field_cooking_time}
                    prepTime={recipe.node.field_preparation_time}
                />
            ))}
        </Layout>
    )
}

export default RecipesTemplate;
module.exports = {
    space: [0, 4, 8, 16, 32, 64],
    fonts: {
      body: "-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, sans-serif",
    },
    fontSizes: [12, 14, 16, 20, 24, 32, 48, 64, 72],
    fontWeights: {
        body: 400,
        heading: 700,
        bold: 900,
    },
    lineHeights: {
      body: 1.5,
      heading: 1.25,
    },
    colors: {
      gray: ['#FAFAFA', '#F5F5F5', '#EEEEEE', '#E0E0E0', '#BDBDBD', '#9E9E9E', '#757575', '#616161', '#424242', '#212121'],
      background: "#fff",
      primary: "rebeccapurple",
      modes: {
        dark: {
            text: '#fff',
            background: '#121212',
            primary: '#3cf',
            secondary: '#e0f',
            muted: '#191919',
            highlight: '#29112c',
            gray: ['#212121', '#424242', '#616161', '#757575', '#9E9E9E', '#BDBDBD', '#E0E0E0', '#EEEEEE', '#F5F5F5', '#FAFAFA'],
            purple: '#c0f'
        },
      },
    },
    sizes: {
      default: "90vw",
      content: "50vw",
      max: "540px",
      maxContent: "800px",
    },
    text: {
      heading: {
        backgroundColor: "primary",
        color: "background",
        fontWeight: "heading",
        margin: "0 auto",
        maxWidth: "max",
        padding: 3,
        width: "default",
        a: {
          color: "#fff",
          'text-decoration': 'none',
        },
        cardHeading: {
            backgroundColor: 'none',
            color: '#fff',
            fontWeight: 'bold',
            margin: "0 auto",
            maxWidth: "max",
            padding: 3,
            width: "default",
            a: {
                color: '#fff',
                'text-decoration': 'none',
            },
        }
      },
    },
    layout: {
      container: {
        margin: "0 auto",
        maxWidth: "maxContent",
        width: "content",
        padding: 3,
        color: "gray.8",
        fontFamily: "body",
        fontSize: 1,
        lineHeight: "body", 
      },
      header: {
        backgroundColor: 'gray.0',
      },
      footer: {
        backgroundColor: 'gray.1',
      },
    },
    images: {
        article: {
            overflow: "hidden",
            width: "100%",
            'border-radius': '8px'
        }
    },
    buttons: {
        colorMode: {
                transition: "all 0.3s linear",
                margin: "8px 0 0 8px",
                fill: "none",
        }
    },
    styles: {
      h1: {
        color: "gray.9",
        fontSize: 5,
        fontWeight: "bold",
        lineHeight: "heading",
        margin: "1rem 0 0",
      },
      ul: {
        borderTop: "1px solid",
        borderColor: "gray.0",
        listStyle: "none",
        padding: 0,
      },
      li: {
        borderBottom: "1px solid",
        borderColor: "gray.4",
        padding: 2,
        "&:focus-within,&:hover": {
          backgroundColor: "gray.4",
        },
      },
      a: {
        'text-decoration': 'none',
        color: '#fff',
      },
      navlink: {
        color: 'gray.7',
        fontFamily: 'body',
        fontSize: 1,
        lineHeight: 'body',
        fontWeight: 'body',
      }
    },
}